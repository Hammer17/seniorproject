from pymongo import MongoClient, errors

class DB():
    def __init__(self, props:dict = {}):
        self.DB_NAME = props.get("DB_NAME", "seniorProjectcluster")
        self.USERNAME = props.get("USERNAME", "admin")
        self.PASSWORD = props.get("PASSWORD", "seniorProject1!")
        self.DB_HOST = props.get("DB_HOST", "wztvg.mongodb.net")

        self.connection = self.establish_connect()
    
    def __del__(self):
        if self.connection and self.connection.client:
            self.connection.client.close()
            print("MongoDB connection is closed")
        else:
            print("unable to close connection")
    
    def establish_connect(self):
        try:
            connection_string = "mongodb+srv://%s:%s@%s.%s/test?retryWrites=true&w=majority"  % (self.USERNAME, self.PASSWORD, self.DB_NAME, self.DB_HOST)
            print(connection_string)
            print("mongodb+srv://admin:seniorProject1!@seniorprojectcluster.wztvg.mongodb.net/test?authSource=admin&replicaSet=atlas-s5xrgd-shard-0&readPreference=primary&appname=MongoDB%20Compass&ssl=true")

            mongoClient = MongoClient( connection_string
                            )
            db = mongoClient.get_database(self.DB_NAME)
            return db

        except errors.ConnectionFailure as e:
            print("Error while connecting to MongoDB", e)
    
    def add_student(self, id, pw, fn, ln):
        new_student={
            'idStudent':id, 
            'passwordStudent':pw,
            'fnameStudent':fn,
            'lnameStudent':ln
        }
        student_col = self.connection.get_collection("student")
        student_col.insert_one(new_student)
        print("wrote student to db")
    
    def get_student_by_login_info(self, id, pw):
        student_col = self.connection.get_collection("student")
        res = student_col.find_one({"idStudent":id, "passwordStudent":pw})

        return res

    def add_course_to_student(self, id, sem, course, completionstatus):
        new_ssc_table={
            'idStudent':id, 
            'idCourse':course,
            'idSemester':sem,
            'completionStatus':completionstatus
        }
        course_col = self.connection.get_collection("ssc_table")
        course_col.insert_one(new_ssc_table)
        print("wrote ssc_table to db")


    
    
    
    

