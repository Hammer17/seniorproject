import "./TopBanner.css";
const TopBanner = (props) => {
  return (
    <div className="TopBanner">
      <h1>{props.BannerText}</h1>
    </div>
  );
};
export default TopBanner;
