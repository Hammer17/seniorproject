import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import Api from "./Api";
import reportWebVitals from "./reportWebVitals";
import NavigationPane from "./NavigationPane";
import { Route, Link, BrowserRouter as Router } from "react-router-dom";
import TopBar from "./TopBar/TobBar";
import Overview from "./Students/Overview/Overview";
import DegreeAudit from "./Students/DegreeAudit/DegreeAudit";
import Schedule from "./Students/Schedule/Schedule";

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
