import { computeHeadingLevel } from "@testing-library/dom";
import LuLogo from "../LU.png";
import { useState } from "react";
import { useHistory } from "react-router";
import { Redirect } from "react-router-dom";
import "./Login.css";
import axios from "axios";

const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  function login2() {
    axios
      .post("/login", { id: username, pw: password }, { timeout: 8000 })
      .then((response) => {
        console.log(response.data);
      })
      .catch((err) => {
        console.log(err.code);
      });
  }

  function login() {
    axios({
      method: "post",
      url: "/login",
      data: { id: username, pw: password },
    }).then((res) => {
      if (res.ok) {
        alert("awesome");
      } else {
        alert("not good");
      }
    });
  }

  const handleSubmit = async () => {
    const response = await fetch("http://localhost:5000/login", {
      method: "POST",
      cache: "no-cache",
      headers: {
        content_type: "application/json",
        Authorization: "Basic",
      },
      body: JSON.stringify({ id: username, pw: password }),
    });
    return response;
  };

  function getuser() {
    fetch("/getuser", {})
      .then((res) => res.json())
      .then((json) => {
        console.log(json);
        sessionStorage.setItem("token", JSON.stringify(json));
      })
      .then(() => (window.location.href = "/overview"));
  }

  const goToRegister = () => {
    console.log("register");
    fetch("/", {}).then(() => {
      window.location.href = "/register";
    });
  };

  return (
    <div className="login-wrapper-wrapper">
      <div className="rectCont">
        <div className="rectangle"></div>
        <a href="/">
          <img src={LuLogo} alt="LU" className="LUpic" />
        </a>
      </div>
      <div className="login-wrapper">
        <h1>Please Log In</h1>
        <form
          onSubmit={() => {
            login2();
            // getuser();
          }}
        >
          <label>
            <p>Username</p>
            <input
              type="text"
              onChange={(e) => setUsername(e.target.value)}
            ></input>
          </label>
          <label>
            <p>Password</p>
          </label>
          <input
            type="password"
            onChange={(e) => setPassword(e.target.value)}
          ></input>
          <div className="login-button">
            <button type="submit">Login</button>
          </div>

          <div className="register-button">
            <button onClick={() => goToRegister()}>Sign Up</button>
          </div>
        </form>
      </div>
    </div>
  );
};
export default Login;
