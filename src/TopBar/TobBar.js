import "./TopBar.css";

const TopBar = (props) => {
  return (
    <div className="TopBar">
      <div className="TopbarTextLeft">{props.pageName}</div>
      <div className="TopbarTextRight">{props.userName}</div>
        <div className="TopbarPicture" placeholder="profilePic">

        </div>
    </div>
  );
};
export default TopBar;
