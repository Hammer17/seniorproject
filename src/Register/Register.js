import "./Register.css";
import { useReducer, useState } from "react";
import LuLogo from "../LU.png";

const Register = () => {
  const [fname, setFname] = useState("");
  const [lname, setLname] = useState("");
  const [ID, setID] = useState("");

  const [major, setMajor] = useState("");
  const [Advisor, setAdvisor] = useState("");
  const [Username, setUsername] = useState("");
  const [Password, setPassword] = useState("");
  const [typeUser, setTypeUser] = useState("");

  const handleSubmit = () => {
    console.log("here");
    fetch("/register", {
      method: "POST",
      cache: "no-cache",
      headers: {
        content_type: "application/json",
      },
      body: JSON.stringify({
        username: Username,
        pw: Password,
        fn: fname,
        ln: lname,
        id: ID,
        major: major,
        advisor: Advisor,
        typeUser: typeUser,
      }),
    }).then((resp) => {
      console.log(resp);
      if (resp.status == 200) {
        // alert("Successfully registered");

        //redirect to the login page
        window.location.href = "/login";
      } else {
        alert("the username is: user ");
      }
    });
  };

  return (
    <div className="Register">
      <div className="rectCont">
        <div className="rectangle"></div>
        <a href="/">
          <img src={LuLogo} alt="LU" className="LUpic" />
        </a>
      </div>

      <div className="RegisterContainer">
        <p>
          To register for an account, please enter the information requested
          below.
        </p>
        <div className="RegisterForm">
          <form
            onSubmit={() => {
              handleSubmit();
            }}
          >
            <div className="row">
              <div className="col-25">
                <label>First Name</label>
              </div>
              <div className="col-75">
                <input
                  className="input"
                  type="text"
                  onChange={(e) => setFname(e.target.value)}
                ></input>
              </div>
            </div>

            <div className="row">
              <div className="col-25">
                <label>Last Name</label>
              </div>
              <div className="col-75">
                <input
                  className="input"
                  type="text"
                  onChange={(e) => setLname(e.target.value)}
                ></input>
              </div>
            </div>

            <div className="row">
              <div className="col-25">
                <label>ID</label>
              </div>
              <div className="col-75">
                <input
                  className="input"
                  type="text"
                  onChange={(e) => setID(e.target.value)}
                ></input>
              </div>
            </div>

            <div className="row">
              <div className="col-25">
                <label>Major</label>
              </div>
              <div className="col-75">
                <input
                  className="input"
                  type="text"
                  onChange={(e) => setMajor(e.target.value)}
                ></input>
              </div>
            </div>

            <div className="row">
              <div className="col-25">
                <label>Advisor</label>
              </div>
              <div className="col-75">
                <input
                  className="input"
                  type="text"
                  onChange={(e) => setAdvisor(e.target.value)}
                ></input>
              </div>
            </div>

            <div className="row">
              <div className="col-25">
                <label>Username</label>
              </div>
              <div className="col-75">
                <input
                  className="input"
                  type="text"
                  onChange={(e) => setUsername(e.target.value)}
                ></input>
              </div>
            </div>

            <div className="row">
              <div className="col-25">
                <label>Password</label>
              </div>
              <div className="col-75">
                <input
                  className="input"
                  type="password"
                  onChange={(e) => setPassword(e.target.value)}
                ></input>
              </div>
            </div>

            <div className="row">
              <div className="col-25">
                <label>Type of User:</label>
              </div>
              <div className="col-75">
                <label for="StudentUser">Student</label>
                <input
                  style={{ margin: "15px" }}
                  type="radio"
                  id="StudentUser"
                  checked={typeUser === "student"}
                  onChange={() => setTypeUser("student")}
                ></input>

                <label for="AdvisorUser">Advisor</label>
                <input
                  style={{ margin: "15px" }}
                  type="radio"
                  id="AdvisorUser"
                  onChange={() => setTypeUser("advisor")}
                  checked={typeUser === "advisor"}
                ></input>
              </div>
            </div>

            <div>
              <button>Submit</button>
            </div>
          </form>
        </div>
      </div>
      <div></div>
    </div>
  );
};
export default Register;
