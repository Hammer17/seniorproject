import "./App.css";
import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import {
  Route,
  Link,
  BrowserRouter as Router,
  Redirect,
} from "react-router-dom";
import NavigationPane from "./NavigationPane";
import TopBar from "./TopBar/TobBar";
import Overview from "./Students/Overview/Overview";
import DegreeAudit from "./Students/DegreeAudit/DegreeAudit";
import Schedule from "./Students/Schedule/Schedule";
import Home from "./HomePage/Home";
import Login from "./Login/Login";
import Register from "./Register/Register";
import { useState } from "react";

const App = (props) => {
  const getToken = () => {
    const tokenString = sessionStorage.getItem("token");
    const userToken = JSON.parse(tokenString);

    return userToken;
  };
  const [loginToken, setLoginToken] = useState(getToken());
  // if (!loginToken) {
  //   console.log(loginToken);
  //   return (
  //     <Router>
  //       <Route exact path="/" component={() => <Home />} />
  //       <Route exact path="/login" component={() => <Login />} />;
  //       <Route exact path="/overview" component={() => <Login />} />;
  //       <Route exact path="/register" component={() => <Login />} />;
  //       {/* <Route component={() => <Home />} /> */}
  //     </Router>
  //   );
  // }
  return (
    <Router>
      <Route exact path="/" component={() => <Home />} />
      <Route exact path="/login" component={() => <Login />} />
      <Route exact path="/register" component={() => <Register />} />

      <Route
        exact
        path="/Overview"
        component={() => (
          <>
            <NavigationPane />
            <Overview FirstName="John" LastName="Doe" PageName="Overview" />
          </>
        )}
      />

      <Route
        exact
        path="/audit"
        component={() => (
          <>
            <NavigationPane />
            <DegreeAudit
              FirstName="John"
              LastName="Doe"
              PageName="Degree Audit"
              Major="B.S. Computer Science"
            />
          </>
        )}
      />
      <Route
        exact
        path="/schedule"
        component={() => (
          <>
            <NavigationPane />
            <Schedule FirstName="John" LastName="Doe" PageName="Schedule" />
          </>
        )}
      />
    </Router>
  );
};

export default App;
