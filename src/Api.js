import { computeHeadingLevel } from "@testing-library/react";
import React from "react";
import "./Api.css";
class Api extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Type: "",
      Identifier: "",
      Level: 0,
      Credit_Hours: 3,
      Name: "",
      Term: "",
      Prereqs_Courses: [
        {
          Identifier: "",
          Level: 0,
        },
        {
          Identifier: "",
          Level: 0,
        },
        {
          Identifier: "",
          Level: 0,
        },
      ],
      Prereqs_RequiresDeanApproval: false,
      Prereqs_RequiresJuniorStanding: false,
      Prereqs_Requires3GPA: false,
      isFallSemester: false,
      isSpringSemeser: true,
      Identifiers: [
        "",
        "COM",
        "MTH",
        "CSC",
        "AAD",
        "DCSI",
        "GAM",
        "EPP",
        "GE-Math",
        "GE-Social Science",
        "GE-Natural Science",
        "GE-Natural Science Lab",
        "GE-Human Culture: US History/Government",
        "GE-Human Culture: Arts",
        "GE-Human Culture: Literature",
        "GE-Human Culture: World History",
        "GE-Human Culture: World History",
        "GE-Human Culture: Foreign Language/Culture",
        "GE-Human Culture: Religion",
        "GE-Human Culture: Philosophy",
        "GE-Human Diversity",
        "UNIV",
      ],
      Types: [
        "",
        "Freshman Experience",
        "General Education Requirements",
        "Natural and Social Science/Math",
        "Human Culture",
        "Electives",
        "Human Diversity",
        "Major Requiremnts (Core)",
        "Major Requiremnts (Electives)",
        "Free Electives",
      ],
    };
    this.handleTypeChange = this.handleTypeChange.bind(this);
    this.handleIdentifierChange = this.handleIdentifierChange.bind(this);
    this.handleLevelChange = this.handleLevelChange.bind(this);
    this.handleCreditsChange = this.handleCreditsChange.bind(this);
    this.handleNameChange = this.handleNameChange.bind(this);
    this.handleSemesterChange = this.handleSemesterChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleDeanApprovalChange = this.handleDeanApprovalChange.bind(this);
    this.handleJuniorStandingChange = this.handleJuniorStandingChange.bind(
      this
    );
    this.handleGPAChange = this.handleGPAChange.bind(this);
    this.Prereqs_CoursesNameChange = this.Prereqs_CoursesNameChange.bind(this);
  }
  Prereqs_CoursesNameChange(value, index) {}
  handleDeanApprovalChange(event) {
    this.setState({
      Prereqs_RequiresDeanApproval: !this.state.Prereqs_RequiresDeanApproval,
    });
  }
  handleGPAChange(event) {
    this.setState({
      Prereqs_Requires3GPA: !this.state.Prereqs_Requires3GPA,
    });
  }
  handleJuniorStandingChange(event) {
    this.setState({
      Prereqs_RequiresJuniorStanding: !this.state
        .Prereqs_RequiresJuniorStanding,
    });
  }
  handleTypeChange(event) {
    this.setState({ Type: event.target.value });
  }
  handleIdentifierChange(event) {
    this.setState({ Identifier: event.target.value });
  }
  handleLevelChange(event) {
    this.setState({ Level: event.target.value });
  }

  handleCreditsChange(event) {
    this.setState({ Credit_Hours: event.target.value });
  }
  handleNameChange(event) {
    this.setState({ Name: event.target.value });
  }

  handleSubmit(event) {
    fetch("/addcourse", {
      method: "POST",
      cache: "no-cache",
      headers: {
        content_type: "application/json",
      },
      body: JSON.stringify(this.state),
    }).then((json) => {
      if (json.status == 201) {
        alert("Successfully Added Course");
      } else {
        alert("there was an error when processing this request " + json.status);
      }
    });
    event.preventDefault();
  }

  handleSemesterChange(event) {
    if (event.target.value == "Fall") {
      this.setState({ isFallSemester: true });
      this.setState({ isSpringSemeser: false });
      this.setState({ Semester: "Fall" });
    } else {
      this.setState({ isFallSemester: false });
      this.setState({ isSpringSemeser: true });
      this.setState({ Semester: "Spring" });
    }
  }

  render() {
    return (
      <div className="CourseSubmit">
        <form onSubmit={this.handleSubmit}>
          <h2>Submit a Course</h2>
          <label>
            Type:
            <select onChange={this.handleTypeChange}>
              {this.state.Types.map((type) => (
                <option value={type}>{type}</option>
              ))}
            </select>
          </label>
          <br />
          <label>
            Identifier:
            <select onChange={this.handleIdentifierChange}>
              {this.state.Identifiers.map((Identifier) => (
                <option value={Identifier}>{Identifier}</option>
              ))}
            </select>
          </label>
          <br />
          <label>
            Level:
            <input
              type="text"
              value={this.state.Level}
              onChange={this.handleLevelChange}
            />
          </label>
          <br />
          <label>
            Credits:
            <input
              type="text"
              value={this.state.Credits}
              onChange={this.handleCreditsChange}
            />
          </label>
          <br />
          <label>
            Name:
            <input
              type="text"
              value={this.state.Name}
              onChange={this.handleNameChange}
            />
          </label>
          <br />
          <label>
            Spring:
            <input
              type="radio"
              value="Spring"
              onChange={this.handleSemesterChange}
              checked={!this.state.isFallSemester}
            />
          </label>

          <label>
            Fall:
            <input
              type="radio"
              value="Fall"
              onChange={this.handleSemesterChange}
              checked={this.state.isFallSemester}
            />
          </label>
          <br />
          <label>
            <br />
            Prereqs: Courses
            <br />
            {this.state.Prereqs_Courses.map((prereq, index) => (
              <span>
                <label>
                  <select onChange={this.Prereqs_CoursesNameChange(index)}>
                    {this.state.Identifiers.map((Identifier) => (
                      <option value={Identifier}>{Identifier}</option>
                    ))}
                  </select>
                  <label>Prereqs Level:</label>
                  <label>
                    <input
                      type="text"
                      value={this.state.Level}
                      onChange={this.handleLevelChange}
                    />
                  </label>
                </label>
                <br></br>
              </span>
            ))}
            <br />
            <label>Requires Dean approval</label>
            <input
              type="checkbox"
              onChange={this.handleDeanApprovalChange}
            ></input>
            <br></br>
            <label>Requires Junior Standing</label>
            <input
              type="checkbox"
              onChange={this.handleJuniorStandingChange}
            ></input>
            <br></br>
            <label>Requires 3.0 GPA</label>
            <input type="checkbox" onChange={this.handleGPAChange}></input>
          </label>
          <br />
          <input type="submit" value="Submit" />
        </form>
      </div>
    );
  }
}

export default Api;
