import TopBar from "../../TopBar/TobBar";
import TopBanner from "../../TopBanner/TopBanner";
import "./Schedule.css";
// import AuditItem from "../DegreeAudit/AuditItem";
import ScheduleItem from "./ScheduleItem";
import { useState } from "react";

const Schedule = (props) => {
  const svgDims = 30;
  const courses = {
    sem: "Spring 2021",
    course: ["MTH 27100", "CSC 43000", "PHY 30200", "ENGL 17000"],
  };
  const allCourses = [
    {
      name: "Fall 2020",
      course: ["MTH 27100", "CSC 41000", "PHY 30100", "ENGL 15000"],
    },
    {
      name: "Spring 2021",
      course: ["MTH 27500", "CSC 44000", "PHY 35200", "ENGL 17000"],
    },
    {
      name: "Fall 2021",
      course: ["MTH 28000", "CSC 43000", "CHM 30200", "ENGL 19000"],
    },
    {
      name: "Spring 2022",
      course: ["MTH 40000", "CSC 53000", "CHM 31200", "ENGL 20000"],
    },
  ];
  const [activeCourseIndex, setActiveCourseIndex] = useState(0);
  return (
    <div>
      <TopBar
        pageName={props.PageName}
        userName={() => `${props.FirstName}  ${props.LastName}`}
      />
      <div className="mainContentWrapper">
        <TopBanner BannerText={`Your Course Schedules.`} />
        <span
          className="back-btn"
          onClick={() =>
            activeCourseIndex > 0
              ? setActiveCourseIndex(activeCourseIndex - 1)
              : null
          }
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width={svgDims}
            height={svgDims}
            fill="currentColor"
            class="bi bi-arrow-left-square"
            viewBox="0 0 16 16"
          >
            <path
              fill-rule="evenodd"
              d="M15 2a1 1 0 0 0-1-1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2zM0 2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2zm11.5 5.5a.5.5 0 0 1 0 1H5.707l2.147 2.146a.5.5 0 0 1-.708.708l-3-3a.5.5 0 0 1 0-.708l3-3a.5.5 0 1 1 .708.708L5.707 7.5H11.5z"
            />
          </svg>
        </span>
        <span
          className="fwd-btn"
          onClick={() =>
            activeCourseIndex < allCourses.length - 1
              ? setActiveCourseIndex(activeCourseIndex + 1)
              : null
          }
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width={svgDims}
            height={svgDims}
            fill="currentColor"
            class="bi bi-arrow-right-square"
            viewBox="0 0 16 16"
          >
            <path
              fill-rule="evenodd"
              d="M15 2a1 1 0 0 0-1-1H2a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2zM0 2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2zm4.5 5.5a.5.5 0 0 0 0 1h5.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H4.5z"
            />
          </svg>
        </span>
        <span></span>
        <ScheduleItem
          Title={allCourses[activeCourseIndex].name}
          level="parent"
        />
        <>
          {allCourses[activeCourseIndex].course.map((c) => (
            <ScheduleItem Title={c} level="child" />
          ))}
        </>
        <div className={"buttonDeg"}>
        <button className={"planDeg"}>Plan my degree</button>
        <button className={"planDegPdf"}>Export Degree Plan to pdf</button>
        </div>
      </div>
    </div>
  );
};
export default Schedule;
