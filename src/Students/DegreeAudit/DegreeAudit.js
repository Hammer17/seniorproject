import TopBar from "../../TopBar/TobBar";
import TopBanner from "../../TopBanner/TopBanner";
import AuditItem from "./AuditItem";
import { useState, UseState } from "react";

const DegreeAudit = (props) => {
  const [show, setShow] = useState(false);
  const [showFreshmanExperience, setShowFreshmanExperience] = useState(false);
  const [showGECore, setShowGECore] = useState(false);
  const [showNSSSM, setShowNSSSM] = useState(false);
  const [showHC, setShowHC] = useState(false);
  const [showGEELECT, setShowGEELECT] = useState(false);
  const [showHD, setShowHD] = useState(false);

  return (
    <div>
      <TopBar
        pageName={props.PageName}
        userName={() => `${props.FirstName}  ${props.LastName}`}
      />
      <div className="mainContentWrapper">
        <TopBanner BannerText={props.Major} />
        <h2 onClick={() => setShow(!show)}>General Education Requirements</h2>

        <span
          onClick={() => setShowFreshmanExperience(!showFreshmanExperience)}
        >
          <AuditItem
            Title="Freshman Experience"
            Status="completed"
            level="parent"
          />
        </span>
        {showFreshmanExperience ? (
          <AuditItem
            Title="Freshman Experience"
            Status="completed"
            level="child"
          />
        ) : null}
        <span onClick={() => setShowGECore(!showGECore)}>
          <AuditItem
            Title="Core"
            Status="not-completed"
            level="parent"
          ></AuditItem>
        </span>
        {showGECore ? (
          <>
            <AuditItem
              Title="Composition I"
              Status="not-completed"
              level="child"
            />
            <AuditItem
              Title="Composition II"
              Status="not-completed"
              level="child"
            />
            <AuditItem Title="Math" Status="not-completed" level="child" />
            <AuditItem
              Title="US History/Government"
              Status="not-completed"
              level="child"
            />
          </>
        ) : null}
        <span onClick={() => setShowNSSSM(!showNSSSM)}>
          <AuditItem
            Title="Natural and Social Science/Math"
            Status="not-completed"
            level="parent"
          />
        </span>
        {showNSSSM ? (
          <>
            <AuditItem
              Title="Social Science"
              Status="not-completed"
              level="child"
            />
            <AuditItem
              Title="Natural Science with Lab"
              Status="not-completed"
              level="child"
            />
            <AuditItem
              Title="Social or Natural Science Elective"
              Status="not-completed"
              level="child"
            />
            <AuditItem
              Title="Social Science, Natural Science, or Math Elective"
              Status="not-completed"
              level="child"
            />
          </>
        ) : null}
        <span onClick={() => setShowHC(!showHC)}>
          <AuditItem
            Title="Human Culture"
            Status="not-completed"
            level="parent"
          />
        </span>
        {showHC ? (
          <>
            <AuditItem Title="Arts" Status="not-completed" level="child" />
            <AuditItem
              Title="Literature"
              Status="not-completed"
              level="child"
            />
            <AuditItem
              Title="Human Cultures Non-Literature, Non-Arts Elective"
              Status="not-completed"
              level="child"
            />
            <AuditItem
              Title="Human Cultures Elective"
              Status="not-completed"
              level="child"
            />
          </>
        ) : null}
        <span onClick={() => setShowGEELECT(!showGEELECT)}>
          <AuditItem Title="Electives" Status="not-completed" level="parent" />
        </span>
        {showGEELECT ? (
          <>
            <AuditItem
              Title="GE Elective"
              Status="not-completed"
              level="child"
            />
            <AuditItem
              Title="GE Elective"
              Status="not-completed"
              level="child"
            />
          </>
        ) : null}
        <span
          onClick={() => {
            setShowHD(!showHD);
          }}
        >
          <AuditItem
            Title="Human Diversity"
            Status="not-completed"
            level="parent"
          />
        </span>
        {showHD ? (
          <>
            <AuditItem
              Title="Human Diversity"
              Status="not-completed"
              level="child"
            />
            <AuditItem
              Title="Human Diversity"
              Status="not-completed"
              level="child"
            />
          </>
        ) : null}
        <h2>Major Requirements</h2>
        <AuditItem Title="Core" Status="not-completed" level="parent" />
        <AuditItem Title="Electives" Status="not-completed" level="parent" />
      </div>
    </div>
  );
};
export default DegreeAudit;
