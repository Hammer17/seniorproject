import TopBar from "../../TopBar/TobBar";
import "./Overview.css";
import TopBanner from "../../TopBanner/TopBanner";
const Overview = (props) => {
  return (
    <div>
      <TopBar pageName="Overview" userName="John Doe" />

      <div className="mainContentWrapper">
        <TopBanner BannerText={`Welcome Back, ${props.FirstName}`} />
        <div class="container">
          <div class="item">
            <div className="mainHeading">45</div>
            <div className="subHeading">Semester Hours Taken</div>
          </div>
          &nbsp;&nbsp;
          <div class="item">
            <div className="mainHeading">75</div>
            <div className="subHeading">Credit Hours To Complete</div>
          </div>
          &nbsp;&nbsp;
          <div class="item">
            <div className="mainHeading">Dec. 2023</div>
            <div className="subHeading">Expected Graduation</div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Overview;
