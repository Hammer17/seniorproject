import "./Home.css";
const Home = () => {
  return (
    <div className="homeContainer">
      <div className="leftContainer"></div>
      <div className="logo" />
      <div className="StudentButton">
        <a href="/Login">Student</a>
      </div>
      <div className="FacultyButton">
        <a href="/Login">Faculty</a>
      </div>
      <div className="rightContainer"></div>
    </div>
  );
};
export default Home;
